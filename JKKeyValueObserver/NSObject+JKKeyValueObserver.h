//
//  NSObject+JKKeyValueObserver.h
//  JKKeyValueObserver
//
//  Created by Joseph Kain on 10/8/13.
//  Copyright (c) 2013 Joseph Kain. All rights reserved.
//

#import <Foundation/Foundation.h>

@class JKKeyValueObserver;

@interface NSObject (JKKeyValueObserver)
- (JKKeyValueObserver *) jkKeyValueObserverForKeyPath:(NSString *)path
                                            withBlock:(void (^)())block;

- (JKKeyValueObserver *) jkKeyValueObserverForKeyPath:(NSString *)path
                                           withTarget:(NSObject *)target
                                               action:(SEL)selector;
@end
