//
//  JKKeyValueObserver.h
//  JKKeyValueObserver
//
//  Created by Joseph Kain on 10/8/13.
//  Copyright (c) 2013 Joseph Kain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JKKeyValueObserver : NSObject

+ (JKKeyValueObserver *)observerForObject:(NSObject *)object
                               onKeyPath:(NSString *)path
                                withBlock:(void (^)())block;

+ (JKKeyValueObserver *)observerForObject:(NSObject *)object
                               onKeyPath:(NSString *)path
                               withTarget:(NSObject *)target
                               action:(SEL)selector;
@end
