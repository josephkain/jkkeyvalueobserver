//
//  main.m
//  JKKeyValueObserver
//
//  Created by Joseph Kain on 10/8/13.
//  Copyright (c) 2013 Joseph Kain. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JKAppDelegate class]));
    }
}
