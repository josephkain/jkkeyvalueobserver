//
//  JKAppDelegate.h
//  JKKeyValueObserver
//
//  Created by Joseph Kain on 10/8/13.
//  Copyright (c) 2013 Joseph Kain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
