//
//  NSObject+JKKeyValueObserver.m
//  JKKeyValueObserver
//
//  Created by Joseph Kain on 10/8/13.
//  Copyright (c) 2013 Joseph Kain. All rights reserved.
//

#import "NSObject+JKKeyValueObserver.h"
#import "JKKeyValueObserver.h"

@implementation NSObject (JKKeyValueObserver)
- (JKKeyValueObserver *) jkKeyValueObserverForKeyPath:(NSString *)path
                                            withBlock:(void (^)())block {
    return [JKKeyValueObserver observerForObject:self onKeyPath:path withBlock:block];
}

- (JKKeyValueObserver *) jkKeyValueObserverForKeyPath:(NSString *)path
                                           withTarget:(NSObject *)target
                                               action:(SEL)selector {
    return [JKKeyValueObserver observerForObject:self
                                       onKeyPath:path
                                      withTarget:target
                                          action:selector];
}
@end
