//
//  JKKeyValueObserver.m
//  JKKeyValueObserver
//
//  Created by Joseph Kain on 10/8/13.
//  Copyright (c) 2013 Joseph Kain. All rights reserved.
//

#import "JKKeyValueObserver.h"
@interface JKKeyValueObserver ()
@property (strong, nonatomic) void (^block)();
@property (weak, nonatomic) NSObject *observedObject;
@property (strong, nonatomic) NSString *observedPath;
@end

@implementation JKKeyValueObserver
- (id)initWithObservedObject:(NSObject *)object keyPath:(NSString *)path
                       block:(void (^)())block

{
    self = [super init];
    if (self) {
        self.block = [block copy];
        self.observedObject = object;
        self.observedPath = [path copy];
        [object addObserver:self forKeyPath:path options:0 context:nil];
    }
    return self;
}

- (void)dealloc {
    [self.observedObject removeObserver:self forKeyPath:self.observedPath];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context {
    self.block();
}

+ (JKKeyValueObserver *)observerForObject:(NSObject *)object
                                onKeyPath:(NSString *)path
                                withBlock:(void (^)())block {
    return [[self alloc] initWithObservedObject:object keyPath:path block:block];
}

+ (JKKeyValueObserver *)observerForObject:(NSObject *)object
                                onKeyPath:(NSString *)path
                               withTarget:(NSObject *)target
                               action:(SEL)selector {
    return [self observerForObject:object onKeyPath:path withBlock:^{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [target performSelector:selector];
#pragma clang diagnostic pop
    }];
}
@end
