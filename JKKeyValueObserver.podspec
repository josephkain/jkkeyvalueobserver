Pod::Spec.new do |s|
  s.name         = "JKKeyValueObserver"
  s.version      = "0.0.4"
  s.summary      = "A simple wrapper around KVO"

  s.description  = <<-DESC
                   A simple wrapper around KVO.
                   DESC

  s.homepage     = "http://www.antipodalapps.com/JKKeyValueObserver"
  s.license      = { type: 'MIT', file: 'LICENSE.txt' }
  s.author       = { "Joseph Kain" => "joekain@gmail.com" }
  s.platform     = :ios, "5.1"
  s.source       = { :git => "file:///Users/jkain/Documents/Projects/AntipodalApps/JKKeyValueObserver", :commit => "200a34b" }
  s.requires_arc = true
  s.source_files  = "JKKeyValueObserver/*JKKeyValueObserver.{h,m}"
end
